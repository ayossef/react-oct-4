# Auth Redux Update - Lab

## In the Auth Component
We need apply the following:

- Add input field for username
- Add a button that sets the user name and set the value of auth flag to true

## In the Main Component:
We need to apply the following:
- Select the auth flag and use it to show Game and Settings instead of Login

## In the logout
We need to apply the following:
- Call the logout action with the logout button.
- Main component should display the login and hide settings and game