# Basic React Commands

## Installing create-react-app

```bash
sudo npm install -g create-react-app
# -g is used to make it globla
# sudo is used to provide admin privileges
```

## Create the first React App

```bash
npx create-react-app <app_name>

```