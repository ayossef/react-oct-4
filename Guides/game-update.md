# Game Update

## Required
We need the game to be initialized eveytime user clicks on "new game" as follows:

- Input field should be cleared
- Check button should be enabled
- Number of trials should be set to 5
- Message should be cleared