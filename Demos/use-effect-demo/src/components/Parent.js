import React from 'react'
import ChildCounter from './ChildCounter'

function Parent() {
  return (
    <div>
        <h1>Parent</h1>
        <ChildCounter></ChildCounter>
    </div>
  )
}

export default Parent