import React from 'react'
import { useState, useEffect } from 'react'
function ChildCounter() {
    const [c1, setC1] = useState(0)
    const [c2, setC2] = useState(0)
    const [c3, setC3] = useState(0)
    const [everytimeCountr, setEverytimeCountr] = useState(0)
    const [mountingCounter, setMountingCounter] = useState(0)
    const [infinteCounter, setInfinteCounter] = useState(0)
    useEffect(() => {setEverytimeCountr(everytimeCountr+1)},[c1,c2,c3]) // first time and mentioned states only
    useEffect(() => {setMountingCounter(mountingCounter+1)},[]) // first time only
   // useEffect(() => {setInfinteCounter(infinteCounter+1)},) // first time and all states
   // useEffect(() => {setInfinteCounter(infinteCounter+1)},[c1, c2, c3, everytimeCountr, mountingCounter, infinteCounter])// first time and all states
   // useEffect(() => {setC1(c1+1)},[c1]) // first time and mentioned states only
    
    return (
    <div>
        <h3>ChildCounter</h3>
        <p>Counter One Value is : {c1}</p>
        <button onClick={() => { setC1(c1+1) }}>Inc C1</button>
        <p>Counter Two Value is : {c2}</p>
        <button onClick={() => { setC2(c2+1) }}>Inc C2</button>
        <p>Counter Three Value is: {c3}</p>
        <button onClick={() => { setC3(c3+1) }}>Inc C3</button>
        <p>This component has been mounted and rerended {everytimeCountr} times</p>
        <p>This component has been mounted {mountingCounter} times</p>
        <p>Infinte Counter is {infinteCounter}</p>
    </div>
  )
}

export default ChildCounter