import React from 'react'
import GuessGame from './GuessGame'
import { useState } from 'react'
function GameBoard() {
    const [magicNumber, setMagicNumber] = useState(0)
    const genMagicNumer = () => {
        setMagicNumber(Math.round(Math.random() * 10))
        console.log('Magic Number is ' + magicNumber)
    }
    return (
        <div>
            <h1>GameBoard</h1>
            <button onClick={genMagicNumer}>New Game</button>
            <GuessGame num={magicNumber}></GuessGame>
        </div>
    )
}

export default GameBoard