import React from 'react'
import { useState } from 'react';

function GuessGame(props) {
    let userInput = 0
    const [newGame, setNewGame] = useState(true)
    let numberOfTrails = 5
    const [message, setMessage] = useState('')
    let magicNumber = props.num
    const checkAction = () => {
       
        if(userInput > magicNumber){
            setMessage('Guess Lower')
        }else if(userInput < magicNumber){
            setMessage('Guess Higher')
        }else {
            setMessage('Congratulations')
            setNewGame(false)
        }
       
        numberOfTrails -= 1
        if(numberOfTrails === 0){
            setNewGame(false)
        }

        console.log('Number of trials is: '+numberOfTrails)
        console.log('New Game is '+newGame)
     }

  return (
    <div>
        <h1>Guess Game</h1>
        <input placeholder='Enter your guess here'
        onChange={(event)=>{userInput = Number(event.target.value)}}>    
        </input>
        <br/>
        <button onClick={checkAction} disabled={!newGame}>Feeling Lucky</button>
        <p>{message}</p>
    </div>
  )
}

export default GuessGame