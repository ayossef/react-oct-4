import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />); // Arrange
  const linkElement = screen.getByText('Main'); // Act
  expect(linkElement).toBeInTheDocument(); // Assert
});

test('should 1 + 2 = 3', () => { 
  // AAA
  // Arrange
  let x = 1
  let y = 2

  // Act
  let z = x + y

  // Assert
  expect(3).toBe(z)
 })