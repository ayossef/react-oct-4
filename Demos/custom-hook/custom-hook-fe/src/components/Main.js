import React from 'react'
import Home from './Home'
import Products from './Products'

function Main() {
  return (
    <div>
        <h1>Main</h1>
        <Home></Home>
        <Products></Products>
    </div>
  )
}

export default Main