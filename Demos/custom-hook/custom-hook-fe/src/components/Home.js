import React from 'react'
import { useFecth } from '../hooks/useFetch'
function Home() {
    const url = "http://localhost:3000/users"
    const users = useFecth(url)


    return (
        <div>
            <h2>Home</h2>
            {users.map((user) =>
                <div>
                    <p>{user.name}</p>
                    <p>{user.email}</p>
                </div>)}
        </div>
    )
}

export default Home