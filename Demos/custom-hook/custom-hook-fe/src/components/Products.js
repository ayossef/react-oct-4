import React from 'react'
import { useFecth } from '../hooks/useFetch'

function Products() {
  const url = "http://localhost:3000/products"
  const products = useFecth(url)
  return (
    <div>
      <h2>Products</h2>
      <p>
        {products.map((product) => 
        <div>
          <p>{product.name}</p>
          <p>{product.price}</p>
        </div>)}
      </p>
    </div>
  )
}

export default Products