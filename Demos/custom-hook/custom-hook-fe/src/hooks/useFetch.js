import { useState, useEffect } from "react"

export const useFecth = (url) => { 
    const [data, setData] = useState([])
    useEffect(() => {
        fetch(url)
            .then((resp) => resp.json())
            .then((jsonResp) => setData(jsonResp))
    }, [url])
    return data
 }