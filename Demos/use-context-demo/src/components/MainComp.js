import React from 'react'
import CompOne from './CompOne'
import { useState } from 'react'
import { counterContext }  from '../contexts/counterContext'

function MainComp() {
    const [counter, setCounter] = useState(0)
    let storeValue  = {
        storeCounter: counter,
        msg: 'local context data - coming from the MainComp file'
    }
  return (
    <counterContext.Provider value={storeValue}>
    <div>
        <h1>
        MainComp
        </h1>
        <p>{counter}</p>
        <button onClick={() => { setCounter(counter +1) }}>Inc Counter</button>
        <CompOne></CompOne>
    </div>
    </counterContext.Provider>
  )
}

export default MainComp