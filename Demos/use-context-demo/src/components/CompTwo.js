import React from 'react'
import CompThree from './CompThree'
import CompFour from './CompFour'

function CompTwo() {
  return (
    <div>
        <h3>CompTwo</h3>
        <CompThree></CompThree>
        <CompFour></CompFour>
    </div>
  )
}

export default CompTwo