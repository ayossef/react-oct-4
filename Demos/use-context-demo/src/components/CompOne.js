import React from 'react'
import CompTwo from './CompTwo'

function CompOne() {
  return (
    <div>
      <h3>CompOne</h3>
      <CompTwo></CompTwo>
    </div>
  )
}

export default CompOne