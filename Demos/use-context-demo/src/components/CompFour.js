import React from 'react'
import { counterContext } from '../contexts/counterContext' 
import { useContext } from 'react'

function CompFour() {
    const storeValue = useContext(counterContext)
  return (
    <div>
        <h4>CompFour</h4>
        <p>{storeValue.msg}</p>
        <p>{storeValue.storeCounter}</p>
    </div>
  )
}

export default CompFour