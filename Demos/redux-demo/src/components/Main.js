import React from 'react'
import Login from './Login'
import Home from './Home'
import Settings from './Settings'
import { useSelector } from 'react-redux'

function Main() {
    const authFlag = useSelector((state) => state.auth.value )
    return (
        <div>
            <h1>Main</h1>
            {!authFlag && <Login></Login>}
            { authFlag && <Home></Home> }
            { authFlag && <Settings></Settings>}
        </div>
    )
}

export default Main