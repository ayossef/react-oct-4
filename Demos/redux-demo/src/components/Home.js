import React from 'react'
import Game from './Game'

function Home() {
    return (
        <div>
            <h2>Home</h2>
            <Game></Game>
        </div>
    )
}

export default Home