import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { clearScore } from '../redux/scoreSlice'
import { logout } from '../redux/authSlice'

function Settings() {
    const actionsQ = useDispatch()
    const username = useSelector((state) =>  state.auth.username )
    const logoutAction = () => { 
      actionsQ(logout())
     }

     const resetScore = () => { 
      console.log('Clearing the score')
        actionsQ(clearScore())
      }
  return (
    <div>
        <h2>Settings</h2>
        <p> Welcome, {username} </p>
        <button onClick={resetScore}>Clear Score</button>
        <button onClick={logoutAction}>Logout</button>
    </div>
  )
}

export default Settings