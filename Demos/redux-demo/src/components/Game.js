import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { decScore, incScore } from '../redux/scoreSlice'

function Game() {
    const userScore = useSelector((state) => state.score.value )
    const actionsQ = useDispatch()
    const win = () => { 
        actionsQ(incScore())

     }
     const lose = () => { 
        actionsQ(decScore())
      }
    return (
        <div>
            <h3>Game</h3>
            <button onClick={win}>Inc Score</button>
            <button onClick={lose}>Dec Score</button>
            <p>Current User Score is : {userScore} </p>
        </div>
    )
}

export default Game