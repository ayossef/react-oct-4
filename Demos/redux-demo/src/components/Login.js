import React from 'react'
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { login , setUsername} from '../redux/authSlice'

function Login() {
    const actionsQ = useDispatch()
    const [localUserInput, setLocalUserInput] = useState('')
    const loginAction = () => { 
        actionsQ(login())
        actionsQ(setUsername({"username": localUserInput}))
        console.log("Local User Input Value  is "+ localUserInput)
     }
    return (
        <div>
            <h2>Login</h2>
            <input placeholder='Username' onChange={(event)=> setLocalUserInput(event.target.value)}></input>
            <button onClick={loginAction}>Login</button>
        </div>
    )
}

export default Login