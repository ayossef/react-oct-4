import authSliceReducer from './authSlice'
import scoreSliceReducer from './scoreSlice'
import {combineReducers, configureStore} from '@reduxjs/toolkit'

let reducer = combineReducers({
    score: scoreSliceReducer,
    auth: authSliceReducer
})


export default configureStore({
    reducer
})