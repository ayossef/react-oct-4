import { createSlice } from '@reduxjs/toolkit'
export const score = createSlice({
    name:'score',
    initialState:{
        value: 0
    },
    reducers:{
        incScore: (state) => { state.value = state.value + 1 },
        decScore: (state) => { state.value = state.value - 1 },
        clearScore: (state) => { state.value = 0 }
    }
})

export const {incScore, decScore, clearScore} = score.actions
export default score.reducer