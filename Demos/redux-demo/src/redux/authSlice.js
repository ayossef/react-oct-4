import { createSlice } from '@reduxjs/toolkit'
export const authSlice = createSlice({
    name:'auth',
    initialState:{
        value: false,
        username: ''
    },
    reducers:{
        login: (state) => { state.value = true },
        logout: (state) => { state.value = false; state.username = '' },
        setUsername: (state, action) => { state.username =  action.payload.username; console.log(JSON.stringify(action.payload))}
    }
})

export const {login, logout, setUsername} = authSlice.actions
export default authSlice.reducer